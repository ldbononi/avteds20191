function reqServlet(json, url, callback) {
	
	var xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			callback(this.responseText);
		}
	};
	
	xhttp.open("POST", url, true);
	xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
	
	if(json != null){
		xhttp.send(JSON.stringify(json));
	}else{
		xhttp.send();
	}

}

var Cliente = {
		addCliente: function(e){
			e.preventDefault();
			
			var t = {};
			t.nome = document.getElementById("nome").value;
			t.cpf = document.getElementById("cpf").value;
			
			reqServlet(t, "addcliente", Cliente.callbackAddCliente);
			
			return false;
		},

		callbackAddCliente: function(responseText){
			var resp = JSON.parse(responseText);
			var msg = document.getElementById("msg");
			msg.style.display = "block";
			msg.innerHTML = resp.msg;
		}
}